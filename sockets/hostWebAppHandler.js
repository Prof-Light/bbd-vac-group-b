// This script handles incoming socket events from the game 'host' laptop client
var dispatch = (..._) => {};

function setDispatch(dispatchFunc) {
    dispatch = dispatchFunc;
}

const { GameState, gameStates } = require('../gamestate/state');

const sendCommands = {
};

const receiveEvents = {
    // This is where we initialize a new game state for each new host
    "hostConnected": (params) => {
        dispatch("player", "all", "questionChanged", "HOST CONNECTED: " + params.data.hostName);
        gameStates[params.data.hostName] = new GameState(dispatch, params.data.hostName);
        gameStates[params.data.hostName].teamManager.setNumTeams(2);
    },
    "startGame": (params) => {
        if (gameStates[params.clientData.hostName]) {
            gameStates[params.clientData.hostName].startGame();
        }
    }
};

module.exports = {
    sendCommands,
    receiveEvents,
    setDispatch
};