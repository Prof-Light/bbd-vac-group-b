//server script
// This script handles incoming socket events from the player web app client
var dispatch = (..._) => {};

function setDispatch(dispatchFunc) {
    dispatch = dispatchFunc;
}

const { gameStates, Player } = require('../gamestate/state');

// Commands that you can send to the player client
const sendCommands = {
    "questionChanged": (newQuestion) => {
        return newQuestion;
    },
    "pageChangeState": (newState) => {
        return newState;
    },
    "reloadPage": (_) => {
        return "";
    }
};

// Events that you can receive from the player client
const receiveEvents = {
    "playerConnect": (data) => {
        const hostName = data.clientData.connectedHost;
        const newPlayer = new Player(
            data.clientData.playerId,                       // playerId
            data.clientData.playerId.replace('p', 'P'),     // username
            data.clientData.socketId                        // socketId
        );

        console.log("PLAYER CONN TO", hostName);

        if (!gameStates[hostName]) {
            console.error("Host not initialized: ", hostName);
            return;
        }
        gameStates[hostName].teamManager.addPlayer(newPlayer);

        return data;
    },
    "userAnswered": (answerChosen) => {
        // The return value here is what will be sent to the server
        // These functions are where you can do any necessary preprocessing
        // console.log("USER ANSWERED", answerChosen);
        const hostName = answerChosen.clientData.connectedHost;
        if (!gameStates[hostName] || !gameStates[hostName].responseCollector) {
            console.error("Host not initialized: ", hostName);
            return;
        }
        gameStates[hostName].responseCollector.respond(
            answerChosen.clientData.playerId,
            answerChosen.clientData.socketId,
            answerChosen.data
        );

        return answerChosen;
    },
}

module.exports = {
    receiveEvents,
    sendCommands,
    setDispatch
};