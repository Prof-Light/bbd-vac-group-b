var dispatch = (..._) => {};

function setDispatch(dispatchFunc) {
    dispatch = dispatchFunc;
}

const sendCommands = {
    // change which direction the sphero is facing
    "moveSphero": (degrees) => {
        return degrees;
    }
};