const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
	res.render('host.ejs');
});

router.use((req, res, next) => {
	res.status(404).send('ERROR 404');
});

module.exports = router;
