The Duel Engine is in charge of two major functions:
1. Updating the scores based on how many cones have been knocked over
2. Starting and stopping the ball control instances/states

The inputs required by the script include:
1. numPins: This is an integer containing how many pins are in the game
2. currentTeam: This variable is a team object that allows us to update the teams score
3. A start instance indicator once the player should be granted control of the ball
4. playerID: This variable is a string that stores the current players ID
5. pinPointValue: This is a variable that has the point value that knocking down one pin should award
6. controlTimeLength: This is a variable that represents how many seconds a player should control the ball for

Functions include:
1. constructor: The parameters for this function are (PinPointValue, controlTimeLength): It has no output: This function essentially creates a Duel Engine object, it instantiates the variables
2. startDuel: The parameters for this function are (CurrentTeam, PlayerID, NumPins): There is no output: This sets variables to the appropriate values for the player to start controlling the ball, it also starts the timer
3. updatePoints: The parameters for this function are (NumPins): There is no output: This function basically compares the new value of NumPins to the old value to update the scores accordingly
4. checkDuelState: The parameters for this function are NumPins: The output is a boolean saying whether the duel should end or not: This function checks the timer and number of pins in the field to decide whether a duel should end or not.
5. getDuelTimeRemaining: This returns the time left before the duel session ends