const defaultQuestions = 
    [
        {
          time: 15,
          question: "What is the capital of France?",
          answers: ["Paris", "London", "Berlin", "Rome"],
          correctAnswer: 0,
          questionsLeft: 9,
        },
        {
          time: 15,
          question: "Which planet is known as the Red Planet?",
          answers: ["Venus", "Mars", "Jupiter", "Saturn"],
          correctAnswer: 1,
          questionsLeft: 8,
        },
        {
          time: 15,
          question: "Who painted the Mona Lisa?",
          answers: ["Vincent van Gogh", "Pablo Picasso", "Leonardo da Vinci", "Michelangelo"],
          correctAnswer: 2,
          questionsLeft: 7,
        },
        {
          time: 15,
          question: "What is the largest ocean in the world?",
          answers: ["Atlantic Ocean", "Indian Ocean", "Arctic Ocean", "Pacific Ocean"],
          correctAnswer: 3,
          questionsLeft: 6,
        },
        {
          time: 15,
          question: "What is the chemical symbol for the element gold?",
          answers: ["Au", "Ag", "Fe", "Hg"],
          correctAnswer: 0,
          questionsLeft: 5,
        },
        {
          time: 15,
          question: "Who wrote the play 'Romeo and Juliet'?",
          answers: ["William Shakespeare", "Arthur Miller", "Henrik Ibsen", "Tennessee Williams"],
          correctAnswer: 0,
          questionsLeft: 4,
        },
        {
          time: 15,
          question: "Which animal is known as the 'King of the Jungle'?",
          answers: ["Lion", "Elephant", "Tiger", "Giraffe"],
          correctAnswer: 0,
          questionsLeft: 3,
        },
        {
          time: 15,
          question: "What is the tallest mountain in the world?",
          answers: ["Mount Everest", "K2", "Kangchenjunga", "Makalu"],
          correctAnswer: 0,
          questionsLeft: 2,
        },
        {
          time: 15,
          question: "Who is the inventor of the telephone?",
          answers: ["Alexander Graham Bell", "Thomas Edison", "Nikola Tesla", "Albert Einstein"],
          correctAnswer: 0,
          questionsLeft: 1,
        },
        {
          time: 15,
          question: "Which country is famous for its tulips?",
          answers: ["Netherlands", "Italy", "Japan", "France"],
          correctAnswer: 0,
          questionsLeft: 0,
        }, 
    ];

module.exports = {defaultQuestions}