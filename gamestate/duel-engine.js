module.exports = class Duel_Engine
{
    //constructor with parameters setting the point value for knocking over a pin, as well as the length of time a player can control the sphere
    constructor(pinPointValue, controlTimeLength)
    {
        this.currentTeam = null
        this.playerID = ""
        this.prevNumPins = 0
        this.pinPointValue = pinPointValue
        this.controlTimeLength = controlTimeLength
        this.duelEnd = true
        this.timerEnded = true
        this.pinsZero = true
        this.startTime = new Date()
        this.arrPrevPins = []
        this.numPinsToSample = 30
    }

    //Called when the quiz engine signals that a team has a streak and that a duel needs to start
    startDuel = (currentTeam, playerID, numPins) =>
    {
        this.currentTeam = currentTeam;
        this.playerID = playerID
        this.prevNumPins = numPins
        this.duelEnd = false
        this.timerEnded = false
        this.pinsZero = false
        this.startTime = new Date()
        this.arrPrevPins = []
    }

    //Needs to be called every time a new frame is processed from the webcam
    updatePoints = (numPins) =>
    {
        this.currentTeam.addPoints(this.playerID, this.pinPointValue * (this.prevNumPins - numPins))
        this.prevNumPins = numPins

        //Add current number of pins to sample array
        this.arrPrevPins.push(numPins)

        if (this.arrPrevPins.length >= this.numPinsToSample)
        {
            var arrTemp = []
            for (let i  = 1; i < this.arrPrevPins.length - 1; i++)
            {
                arrTemp[i - 1] = this.arrPrevPins[i]
            }
            this.arrPrevPins = arrTemp
        }

        var arrayIsZero = true
        for (let i = 0; i < this.arrPrevPins.length; i++)
        {
            //Check that all values are equal
            if (this.arrPrevPins[i] != 0)
                arrayIsZero = false
        }
        //Assign end condition
        this.pinsZero = arrayIsZero
    }

    //Called after updatePoints()
    checkDuelState = () =>
    {
        //Elapsed time in seconds
        var elapsedTime = (new Date() - this.startTime) / 1000

        //If elapsed time exceeds controlTimeLength end duel
        if (elapsedTime >= this.controlTimeLength)
            this.timerEnded = true

        //If either the timer has ended or the pins are all gone end the duel
        this.duelEnd = (this.timerEnded || this.pinsZero)
        return this.duelEnd
    }

    //Returns the time remaining in the duel for display on the GM screen
    getDuelTimeRemaining = () =>
    {
        return (this.controlTimeLength - (new Date() - this.startTime) / 1000)
    }
}
