const crypto = require("crypto");
const { QuizState } = require("./quiz");
const { response } = require("express");

class GameState {
    // * dispatch: (socketId: string, commandName: string, data: any) => void
    constructor(dispatch, connectedHostName) {
        this.connectedHostName = connectedHostName;
        this.teams = [];
        this.quizState = new QuizState(this);
        this.dispatch = dispatch;
        this.teamManager = new TeamManager(dispatch, this);
        this.responseCollector = null;
        this.gameIsRunning = false;
    }

    // Called when host sends "startGame"
    // Runs the main game loop
    async startGame() {
        this.gameIsRunning = true;
        console.log("STARTING GAME");
        console.log(this.teamManager.teams);

        // host "pageChangeState": 'gamePage', 'lobby', 'digitalMap', 'quizScreen'

        // Start quiz + obtain number of rounds
        // const numRounds = this.quizState.getRoundsInGame();
        const numRounds = 1;
        
        for (let round = 0; round < numRounds; round++) {

            // Change host state to "quiz"
            this.dispatch("host", "all", "pageChangeState", "quiz"); //
            // Alert host of round number
            this.dispatch("host", "all", "roundChanged", round + 1); // +1 because rounds are 1-indexed

            // const numQuestions = this.quizState.getQuestionsPerRound();
            const numQuestions = 2;

            for (let q = 0; q < numQuestions; q++) {

                const question = this.quizState.getCurrentQuestion();

                // Change all player states to "answering"
                this.dispatch("player", "all", "pageChangeState", "quiz-container");
                this.dispatch("host", "all", "pageChangeState", "gamePage");

                // Send question to all players + host
                this.dispatch("player", "all", "questionChanged", question);
                this.dispatch("host", "all", "questionChanged", question); //TODO: Only send to this.hostName

                // ... Wait for all players to answer + collect responses
                this.responseCollector = new PlayerResponseCollector(this.dispatch, this.teamManager, question.time);
                const responses = await this.responseCollector.collectPlayerResponses();
                console.log("RESPONSES", responses);
                this.responseCollector = null;

                // Send responses to quizState to verify + update scores
                const bestPlayerThisRound = this.quizState.processQuizScores(responses);

                // Send updated scores to host for display
                const scoreData = this.teamManager.getTeamScores();
                this.dispatch("host", "all", "scoresChanged", scoreData);
            }

            // Obtain best player for the round
            const bestPlayerThisRound = this.quizState.getBestPlayer(round);

            // Begin new duel
                // Change all player states to "waiting"
                this.dispatch("player", "all", "pageChangeState", "waiting-page");
                // Change specific duelling player state to "duelling"
                // this.dispatch("player", bestPlayerThisRound.playerId, "pageChangeState", "joystick-container");

                // > Stream controller input from duelling player to host

                // ??? Call duel functionality linked with sphero host ???

                // ... Wait for duel to finish


            // ... Wait for host to start next round
            // Then:
            // this.quizState.nextRound(); // TODO: This doesn't exist yet
        }

        // Calculate winner + final scores & send to host for display
        this.gameIsRunning = false;
        this.dispatch("player", "all", "reloadPage", null);
    }
}

class PlayerResponseCollector {
    constructor(dispatch, teamManager, questionDuration = 15) {
        this.dispatch = dispatch;
        this.teamManager = teamManager;
        this.questionDuration = questionDuration;
        this.responses = [];
    }

    /*
      * returns: {
          teamId: number,
          playerID: string,
          submissionTime: Date
          answerIndex: number
      }[]
    */
    async collectPlayerResponses() {
        // Wait for questionDuration seconds
        console.log("===== WAITING =====")
        await new Promise(r => setTimeout(r, 1000 * this.questionDuration));
        console.log("===== DONE WAITING =====")
        return this.responses;
    }

    respond(playerId, socketId, answerChosen) {
        console.log("++++++++++++++++++ SOMEONE RESPONDED +++++++++++++++++++");
        const playerTeam = this.teamManager.getPlayerTeam(playerId); // teamId
        console.log("PLAYER", playerId);
        console.log("TEAM", playerTeam);
        console.log("TEAM PLAYERS", this.teamManager.teams[playerTeam].players);
        if (playerTeam !== null) {
            this.responses.push({
                teamId: playerTeam,
                playerID: playerId,
                submissionTime: Date.now(),
                answerIndex: answerChosen
            });
            this.dispatch("player", socketId, "pageChangeState", "waiting-page");
        }
        else {
            console.error("Player tried responding to question but was not in a team");
        }
    }
}

class TeamManager {
    constructor(dispatch, gameState) {
        this.gameState = gameState;
        this.dispatch = dispatch;
        this.teams = []; // Items of type Team

        // The lobby stores all players who are waiting to be assigned to a team
        this.lobby = []; // Items of type Player
    }

    // * returns: number (teamId)
    getPlayerTeam(playerId) {
        for (let t = 0; t < this.teams.length; t++) {
            if (this.teams[t].checkHasPlayer(playerId)) {
                return t;
            }
        }
        return null;
    }

    // * returns: { teamNo: number, teamColour: cssString, score: number }[]
    getTeamScores() {
        const scores = [];

        for (let t = 0; t < this.teams.length; t++) {
            const team = this.teams[t];
            const score = team.getTotalPoints();
            scores.push({ teamNo: team.teamNo, teamColour: team.colour, score: score });
        }

        return scores;
    }

    // Set the number of teams and shuffle players amongst them
    // * numTeams: number > 1
    setNumTeams(numTeams) {
        if (this.teams.length !== 0) {
            // There are existing teams, so we need to shuffle + redistribute players

            // Append all players to lobby
            for (let t = 0; t < this.teams.length; t++) {
                for (let p in this.teams[t].players) {
                    this.lobby.push(this.teams[t].players[p]);
                }
            }

            // Create new teams
            this.teams = [];
            for (let i = 0; i < numTeams; i++) {
                this.teams.push(new Team(i));
            }

            // Assign all players in lobby to a team
            this.distributeLobbyToTeams();
            // // const numTeams = this.teams.length;
            // for (let p = 0; p < this.lobby.length; p++) {
            //     // const teamNo = p % numTeams;
            //     this.addPlayer(this.lobby.pop());
            // }

        }
        else {
            // No teams currently exist, create them
            for (let i = 0; i < numTeams; i++) {
                this.teams.push(new Team(i));
            }

            this.distributeLobbyToTeams();
        }
    }

    distributeLobbyToTeams() {
        // Assign all players in lobby to a team
        for (let p = 0; p < this.lobby.length; p++) {
            const player = this.lobby.pop();
            this.addPlayer(this.lobby.pop());
        }
    }

    // Returns the index of the team in `teams`
    // * returns: number[]
    getTeamsWithLeastPlayers() {
        if (this.teams.length == 0) {
            return [];
        }

        let minPlayers = this.teams[0].getNumPlayers();
        let minTeamIndices = [0];
        for(let t = 1; t < this.teams.length; t++) {
            let numPlayers = this.teams[t].getNumPlayers();

            if (numPlayers < minPlayers) {
                minPlayers = numPlayers;
                minTeamIndices = [t];
            }
            else if (numPlayers == minPlayers) {
                minTeamIndices.push(t);
            }
        }

        return minTeamIndices;
    }

    // Automatically adds a player to a team with least players, make sure there are existing teams
    // * player: Player
    addPlayer(player) {

        let teamsWithLeastPlayers = this.getTeamsWithLeastPlayers();

        if (!this.gameState.gameIsRunning && teamsWithLeastPlayers.length > 0) {
            const randomIndex = Math.floor(teamsWithLeastPlayers.length * Math.random());
            const teamNo = teamsWithLeastPlayers[randomIndex];

            this.teams[teamNo].addPlayer(player);
        }
        else
        {
            this.lobby.push(player);
            // Notify host that a player has joined the lobby
            this.dispatch("host", "all", "playerJoinedLobby", player);
            this.dispatch("host", "all", "lobbyPlayerUpdate", this.lobby.forEach(player => player.username));
        }

    }
}

class Team {
    // * colour: TeamColour
    constructor(teamNo) {
        this.colour = TeamColours[Math.floor(TeamColours.length * Math.random())];
        this.teamNo = teamNo;

        // `players` and `points` are both indexed by playerId
        // This way if a player loses connection we still have their points
        this.players = {};
        this.points = {};
    }

    checkHasPlayer(playerId) {
        return this.players[playerId] !== undefined;
    }

    getNumPlayers() {
        return Object.keys(this.players).length;
    }

    // * returns: Team
    // randomTeam(randomTeamNo) {
    //     const colour = TeamColours[Math.floor(TeamColours.length * Math.random())];
    //     return new Team(colour, randomTeamNo);
    // }

    // * teamNo: number
    // * returns: playerId
    createPlayer(username) {
        const playerId = crypto.randomBytes(16).toString("hex");
        const newPlayer = new Player(playerId, username, this.teamNo);

        this.players[playerId] = newPlayer;

        return playerId;
    }

    // Points are stored individually per player
    // * returns: number
    getTotalPoints() {
        let total = 0;
        for (const p in this.players) {
            total += p.points;
        }
        return total;
    }

    // * playerId: string
    // * points: number
    addPoints(playerId, points) {
        if (!this.points[playerId]) {
            this.points[playerId] = 0;
        }
        this.points[playerId] += points;
    }

    // * player: Player
    addPlayer(player) {
        this.players[player.playerId] = player;
    }

    // * playerId: string
    getPlayer(playerId) {
        return this.players[playerId];
    }
}

class Player {
    // * playerId: string
    // * username: string
    // * team: number - index of team in team array in TeamManager
    constructor(playerId, username, socketId) {
        this.playerId = playerId;
        this.username = username;
        this.socketId = socketId;
    }
}

// These should be CSS colour strings
const TeamColours = [
    "red",
    "blue",
    "green",
    "yellow"
];

// Maps a host id to a game state
// If we have multiple hosts they will each index
// into here
const allGameStates = {}

module.exports = { GameState, Player, gameStates: allGameStates, Team, TeamManager };