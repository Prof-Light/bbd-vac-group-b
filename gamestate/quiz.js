const {defaultQuestions}  = require("../util/defaultQuestions.js");

class QuizState {  
    static DEFAULT_SCORE = 100;
    static NUM_QUESTIONS = 10;
    static NUM_ROUNDS = 5;
    static DEFAULT_QUESTION_TIME = 15;
    static EASY = 15;
    static MEDIUM = 25;
    static HARD = 35;

    // Example question format for testing:
    static DEFAULT_QUESTIONS = defaultQuestions;
    
    /*
      Constructor allows for some customisation of the quiz, with set defaults
    */
   // TODO: Add customisation of difficulty marks
    constructor(
      gameState, 
      question_score = QuizState.DEFAULT_SCORE, 
      questions_per_round = QuizState.NUM_QUESTIONS, 
      rounds = QuizState.NUM_ROUNDS
      ) {
      // Save gameState for score updates
      this.gameState = gameState;
      this.currentQuestion = 0;
      this.currentRound = 0;
      this.question_score = question_score;
      this.questions_per_round = questions_per_round;
      this.rounds = rounds;
      this.questions = [];

      // Init questions
      this.newGame();
    }

    /*
      Function verifies a single player's answer against currentQuestion
      and assigns them a score based on how quickly they answered
      * timeTaken - how long the player took to answer
      * answerIndex - index of the answer in the question.answers array
    */
    verifyQuestion(timeTaken, answerIndex) {
      if (this.questions.length <= this.currentQuestion) { return; }

      const currentQuestion = this.questions[this.currentQuestion];

      const multiplier = (currentQuestion.time - timeTaken) / currentQuestion.time;
    
      if (answerIndex === currentQuestion.correctAnswer)
      {
        // Answered correctly
        return Math.round(this.question_score * multiplier);
      }
      else{
        return 0;
      }
    }

    // Check all player answers, update scores, and move to the next question
    /*
      * playerAnswers: {
          teamId: number,
          playerId: string,
          submissionTime: Date
          answerIndex: number
      }[]
    */
    processQuizScores(playerAnswers, initialTime) {
        if (playerAnswers.length === 0) { return; }

        let bestPlayerScore = 0;
        let bestPlayer = playerAnswers[0].playerId;

        // Loop through all player answers and update their scores
        for (let answer of playerAnswers) {
            const timeTaken = answer.submissionTime - initialTime;
            const pointsAllocated = this.verifyQuestion(timeTaken, answer.answerIndex);

            // Check for best player (highest score even if everyone got it wrong)
            if (pointsAllocated > bestPlayerScore) {
              bestPlayerScore = pointsAllocated;
              bestPlayer = answer.playerId;
            }

            if (this.gameState.teamManager.teams[answer.teamId]) {
              this.gameState.teamManager.teams[answer.teamId].addPoints(answer.playerId, pointsAllocated);
            }
        }


        // Assuming that information about the "current" question is no longer needed once the scores are updated,
        // the current question is incremented here
        this.currentQuestion++;
        this.currentQuestion %= this.questions.length;

        return bestPlayer;
    }

    /*
      format of questions:
      {
          Time: How long a question should have to be answered (in seconds),
          question: The current question,
          answers: An array containing the 4 possible answers,
          correctAnswer: The index of the correct answer in the answers array,   
          questionsLeft: How many questions are left in the current round (where a round contains 10 questions),   
      }
    */
    /*
     You don't need me to explain this one
    */
    getCurrentQuestion() {
        return this.questions[this.currentQuestion];
        return [
        {
            time: QuizState.DEFAULT_QUESTION_TIME,
            question: "What is the answer to life, the universe, and everything?",
            answers: ["6", "9", "420", "42"],
            correctAnswer: 3,
        },
        {
            time: QuizState.DEFAULT_QUESTION_TIME,
            question: "Why did the chicken cross the road?",
            answers: ["Yes", "To get to the other side", "No bitches", "42"],
            correctAnswer: 3,
        },
        {
          time: QuizState.DEFAULT_QUESTION_TIME,
          question: "Honey, where is my super suit?",
          answers: ["What?", "I put it away.", "Where is my super suit?", "Why do you need to know?"],
          correctAnswer: 2,
        },
        ][Math.floor(Math.random() * 3)];

        // Current question will be moved on when updateScores is called
    }

    /*
      Function returns the playerId of the player that did the best in the last round
    */
    //TODO: Function returns best player overall, which is not necessarily the best player in the current round
    getBestPlayer(){
      let maxScore = 0;
      let maxPlayer = 0;

      for (let team of this.gameState.teams) {
        for (let player of team.players) {
          if (player.score > maxScore) {
            maxScore = player.score;
            maxPlayer = player.playerId;
          }
        }
      }

      return maxPlayer;
    }

    /*
      Function wipes the current quiz state and starts a new game
    */
    // TODO: Create and wipe internal state for questions marks, so that best player of each round can be selected
    newGame(){
      this.currentQuestion = 0;
      this.currentRound = 0;
      this.populateQuestions(this.questions_per_round * this.rounds);
    }

    /*
      Function makes an async request to the Open Trivia Database API to populate this.questions
      Data is converted to a standard format before being stored in the questions array
    */
    populateQuestions(questionCount, use_default = true)
    {
      if (use_default) {
        // use_default populates the question array with the default questions, possibly duplicated
        this.questions = QuizState.DEFAULT_QUESTIONS;
      }
      else{ 
        // If not default, make a request to the Open Trivia Database API.
        this.questions = populateFromAPI(questionCount);
      }
    }


    /*
      TODO: Function returns given amount of questions from the Open Trivia Database API, in the convertFormat format.
      Please merge convertFormat into this.
    */
    async populateFromAPI(questionCount) {
      const response = await fetch("https://opentdb.com/api.php?amount="+questionCount+"&type=multiple");
      const body = await response.json();
      this.questions = this.convertFormat(body.results);
    }

    // Function converts the format of the questions from the Open Trivia Database API to a standard format
    convertFormat(data) {
      try {

        for (let i = 0; i < data.length; i++) {
          const current = data[i];
          const q = current.question;
          const options = [current.correct_answer];
          for (let j = 0; j < current.incorrect_answers.length; j++) {
            options.push(current.incorrect_answers[j]);options
          }
          let shuffledOptions = this.shuffleArray(options);
          let obj = {
            // TODO: Change time to not be static 15 seconds
            time: QuizState.DEFAULT_QUESTION_TIME,
            question: q,
            answers: shuffledOptions,
            correctAnswer: shuffledOptions.indexOf(options[0]),
          }
          this.questions.push(obj)
        }
        let shuffledOptions = this.shuffleArray(options);
        let questionTime = 15;
        // TODO: Might change with difficulty in constructor
        if (current.difficulty === "easy") {
          questionTime = QuizState.EASY;
        }
        else if (current.difficulty === "medium") {
          questionTime = QuizState.MEDIUM;
        }
        else if (current.difficulty === "hard") {
          questionTime = QuizState.HARD;
        }


        let obj = {
          // TODO: Change time to not be static 15 seconds
          time: questionTime,
          question: q,
          answers: shuffledOptions,
          correctAnswer: shuffledOptions.indexOf(options[0]),
          // TODO: Change or remove, since it might not be used
          questionsLeft: (this.questions_per_round - i % this.questions_per_round) - 1
        }
        this.questions.push(obj)
      }
      catch (err) {
        console.error(err);
      }
    }

    // Fisher-Yates shuffle
    shuffleArray(array) {
      const newArray = [...array];
      for (let i = newArray.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [newArray[i], newArray[j]] = [newArray[j], newArray[i]];
      }
      return newArray;
    }


    // Getters/Setters
    getRoundsInGame() {
      return this.rounds;
    }

    getQuestionsPerRound() {
      return this.questions_per_round;
    }
}


module.exports = { QuizState };
