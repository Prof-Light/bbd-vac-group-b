# Core Functions of the Quiz Engine
    - generate questions
    - send questions to game master
    - receive player answers and time they answered
    - keep track of team scores and who gets control of the sphero ball