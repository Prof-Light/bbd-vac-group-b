# Scorekeeping
Each team has an overall score which is used to rank them. The following contributes to a teams score:
    - answering a question correctly (with more points awarded for answering in less time)
    - (?) knocking down cones (score than comes from the duel engine subsystem)

The team which answered the quiz question correctly in the least time gets to duel (a chance to control the sphero ball)
