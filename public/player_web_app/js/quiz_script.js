document.addEventListener("DOMContentLoaded", function () {

  // Generate random background color
  var options;

  function setOptions(inOptions) {
    options = inOptions;
  }

  function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  // Set random background color on page load
  document.body.style.backgroundColor = getRandomColor();
});
document.addEventListener("DOMContentLoaded", function () {

  return;
  // The code below is not working properly, I've put my own equivalent impelmentation
  // in playerSocket.js
  // Not sure it's worth trying to revive the below implementation

  var chosen = false;

  document.getElementById("Option1").addEventListener("click", function () {

    if (!chosen) {
      //send answ
      dispatch("userAnswered", options[0]);
      chosen = true;
    }

  });

  document.getElementById("Option2").addEventListener("click", function () {

    if (!chosen) {
      //send answ
      dispatch("userAnswered", options[1]);
      chosen = true;
    }
  });

  document.getElementById("Option3").addEventListener("click", function () {
    var answer = "";
    if (!chosen) {
      //send answ
      dispatch("userAnswered", options[2]);
      chosen = true;
    }
  });

  document.getElementById("Option4").addEventListener("click", function () {
    var answer = "";
    if (!chosen) {
      //send answ
      dispatch("userAnswered", options[3]);
      chosen = true;
    }
  });
});
