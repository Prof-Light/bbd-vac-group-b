var pageStates = {};

window.onload = () => {
    pageStates = { 
        "joystick-container": document.getElementById("joystick-container"), 
        "correct-page": document.getElementById("correct-page"),
        "incorrect-page": document.getElementById("incorrect-page"),
        "waiting-page": document.getElementById("waiting-page"),//needs adjusting
        "winner-page-container": document.getElementById("winner-page-container"),
        "quiz-container": document.getElementById("quiz-container")
    };

    setPageState("waiting-page");
};

// * newPage: string (see above keys)
function setPageState(newPage) {
    // Disable all pages
    for (const page in pageStates) {
        if (!pageStates.hasOwnProperty(page)) continue;

        if (!pageStates[page].classList.contains("hiddenElem"))
            pageStates[page].classList.add("hiddenElem");
            console.log("hidden " + page);
    }

    // Enable new page
    pageStates[newPage].classList.remove("hiddenElem");
    console.log("shown " + newPage);

}