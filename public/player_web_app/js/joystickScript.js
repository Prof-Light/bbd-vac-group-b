let joystick = document.getElementById("joystick");
let joystickKnob = document.getElementById("joystick-knob");

console.log("Got joystick elements");

joystick.addEventListener("touchstart", (event) => {
    joystickKnob.style.transition =
        "transform 0ms ease-in-out, height 0ms ease-in-out";

    const touchX = event.touches[0].clientX;
    const touchY = event.touches[0].clientY;

    const rect = joystick.getBoundingClientRect();

    // Calculate the center point of the div
    const centerX = rect.left + rect.width / 2;
    const centerY = rect.top + rect.height / 2;

    const diffX = touchX - centerX;
    const diffY = touchY - centerY;

    const angle = Math.atan2(diffY, diffX);

    let radius = Math.hypot(diffX, diffY);

    if (radius > joystick.offsetWidth / 2.4) {
        radius = joystick.offsetWidth / 2.4;
    }

    const xTrans = radius * Math.cos(angle);
    const yTrans = radius * Math.sin(angle);

    joystickKnob.style.transform = `translate(${xTrans}px, ${yTrans}px) rotate(${angle - Math.PI / 2
        }rad)`;

    const magnitude = radius / joystick.offsetWidth; // converts the magnitude to a value between 0 and 1
    // const angle_in_degrees = (angle/(2*PI)) * 360;
    

    // !!! emit magnitude and angle !!!
});

console.log("Joystick touchstart event added");

joystick.addEventListener("touchmove", (event) => {
    const touchX = event.touches[0].clientX;
    const touchY = event.touches[0].clientY;

    const rect = joystick.getBoundingClientRect();

    // Calculate the center point of the div
    const centerX = rect.left + rect.width / 2;
    const centerY = rect.top + rect.height / 2;

    const diffX = touchX - centerX;
    const diffY = touchY - centerY;

    const angle = Math.atan2(diffY, diffX);

    let radius = Math.hypot(diffX, diffY);

    if (radius > joystick.offsetWidth / 2.4) {
        radius = joystick.offsetWidth / 2.4;
    }

    const xTrans = radius * Math.cos(angle);
    const yTrans = radius * Math.sin(angle);

    joystickKnob.style.transform = `translate(${xTrans}px, ${yTrans}px) rotate(${angle - Math.PI / 2
        }rad)`;

    const magnitude = radius / joystick.offsetWidth; // converts the magnitude to a value between 0 and 1
    const angle_in_degrees = Math.round(angle / (2 * Math.PI)) * 360 + 180;

    const angle_span = document.getElementById("angle");
    // document.getElementById("angle").innerHTML = "angle   " + angle_in_degrees;
    // document.getElementById("mag").innerHTML = "mag   " + angle_in_degrees;
    

    // !!! emit magnitude and angle !!!
    dispatch('ballMovementAngle', angle_in_degrees, magnitude);
});

joystick.addEventListener("touchend", () => {
    joystickKnob.style.transition =
        "transform 200ms ease-in, height 200ms ease-in";
    joystickKnob.style.transform = "translate(0, 0) rotate(0rad)";

    // !!! make sure to signal that the magnitude and angle are 0 !!!
    dispatch('ballMovementAngle', 0, 0);
    
});

console.log("loaded");