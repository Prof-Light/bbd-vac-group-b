const socket = io("http://localhost:3000");

// For the MVP we'll only have one host running
// TODO: Derive this from URL when the user scans the
//       QR code on the game host's screen
var connectedHostName = "default";

const playerId = "player_" + Math.floor(Math.random() * 1000000);

var timeLeft = 0;
setInterval(() => {
    timeLeft -= 1;
    if (timeLeft < 0) timeLeft = 0;

    const timers = document.getElementsByClassName("timer");
    for (let i = 0; i < timers.length; i++) {
        if (timers[i]) {
            timers[i].innerHTML = (timeLeft == 0 ? "" : `${timeLeft}s`);
        }
    }
}, 1000);

//========== HTML ELEMENTS ==========//
// you are sending in the params for each of these functions :)

const sendCommands = {
    // * answerChosen: "A" | "B" | "C" | "D"
    // * returns: "A" | "B" | "C" | "D"
    "userAnswered": (answerChosen) => {
        console.log("CHOSE ANSWER", answerChosen);
        return answerChosen;
    },
    "playerConnect": (data) => {
        return { ...data, connectedHost: connectedHostName };
    },
    "stateChanged": (state) => {
        return state;//string
    },
    "ballMovementAngle": (angle, mag) => {
        return { direction: angle, magnitude: mag };//integer
    }
};

// These are events that you want to listen for on the client mobile application
// Don't change this at runtime, all events should be defined at the start
const receiveEvents = {
    "questionChanged": (newQuestion) => {
        if (newQuestion.question) {
            const questionText = document.getElementById("questionText");
            questionText.innerHTML = newQuestion.question;
            const answerBtns = [
                document.getElementById("answerA"),
                document.getElementById("answerB"),
                document.getElementById("answerC"),
                document.getElementById("answerD")
            ];
            for (let a = 0; a < 4; a++) {
                answerBtns[a].innerHTML = newQuestion.answers[a];
            }
            timeLeft = newQuestion.time;
        } else { console.error("Question is blank"); }
    },
    "getTeamName": (teamName) => {
        if (newQuestion.length > 0) {
            return teamName;//string
        } else { console.error("Team Name is blank"); }

    },
    "getWinningTeam": (winningTeam) => {
        if (newQuestion.length > 0) {
            return winningTeam;//string
        } else { console.error("Winning Team is blank"); }
    },
    "getTimeForQuiz": (time) => {
        if (time => 0) {
            return time;//float
        } else { console.error("Time is negative"); }
    },
    "pageChangeState": (newPage) => {
        console.log("Changing page to", newPage);
        setPageState(newPage);
    },
    "reloadPage": (_) => {
        window.location.reload();
    }
};


//===== SOCKET CONFIG STUFF =====//
// You can mostly ignore this

var socketId = null;

socket.on('connect', function () {
    console.log('Connected to the server');
    socketId = socket.id;
    socket.emit("ehlo", "player"); // This tells the server that this is a player
    dispatch("playerConnect", { id: socketId });
});

socket.on('disconnect', function () {
    socketId = null;
    console.log('Disconnected from the server');
});

// Auto close the socket connection when the window is closed
window.onbeforeunload = function () {
    socket.disconnect();
    console.log('Socket disconnected due to page unload');
};

function getClientData() {
    return {
        socketId,
        connectedHost: connectedHostName,
        playerId
    }
}

// This is the function that you call to dispatch one of the above sendCommands
function dispatch(commandName, data) {
    console.log("Sending event:", commandName, data);
    if (sendCommands[commandName]) {
        const clientData = getClientData();
        const payload = { clientData, data: sendCommands[commandName](data) };
        console.log("Sending payload:", payload);
        if (payload !== null) {
            socket.emit("player/" + commandName, payload);
        }
    }
}

// Start listening for receiveEvents
for (const eventName in receiveEvents) {
    if (!receiveEvents.hasOwnProperty(eventName)) continue;

    socket.on(eventName, (data) => {
        receiveEvents[eventName](data);
    });
}