const digitalMapCanvas = document.getElementById('digitalMapCanvas');
const ctx = digitalMapCanvas.getContext('2d');

// --- global variables ---

const cones = [];
let sphere;

// --- class definitions ---

// cones will be represented as circles for now...
class Cone {
	constructor(x, y, radius, colour) {
		this.x = x;
		this.y = y;
		this.radius = radius; // will be updated later when images are used for the cones
		this.colour = colour;
		this.state = 'upright'; // not the final data type for the state of the cone
		this.alpha = 1; // opacity of the cone
	}

	draw() {
		ctx.save();
		ctx.globalAlpha = this.alpha;
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
		ctx.fillStyle = this.colour;
		ctx.fill();
		ctx.restore();
	}

	updateState(state) {
		this.state = state;
	}
}

class Sphere {
	constructor(x, y, radius, colour) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.colour = colour;
		this.alpha = 1; // opacity of the cone
	}

	draw() {
		ctx.save();
		ctx.globalAlpha = this.alpha;
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
		ctx.fillStyle = this.colour;
		ctx.fill();
		ctx.restore();
	}

	updatePosition(x, y) {
		this.x = x;
		this.y = y;
	}
}

// --- functions ---

function animateMap() {
	requestAnimationFrame(animateMap);
	ctx.clearRect(0, 0, digitalMapCanvas.width, digitalMapCanvas.height);

	cones.forEach((cone) => {
		cone.draw();
	});

	sphere.draw();
}

function resizeCanvas() {
	digitalMapCanvas.width = innerWidth;
	digitalMapCanvas.height = innerHeight;

	animationCanvas.width = innerWidth;
	animationCanvas.height = innerHeight;

	minRadius = innerWidth / 8;
	maxRadius = innerWidth / 6;
	minSpeed = innerWidth / 10000;
	maxSpeed = innerWidth / 8000;
}

// --- main program logic ---

resizeCanvas();
addEventListener('resize', resizeCanvas);

sphere = new Sphere(Math.random() * innerWidth, Math.random() * innerHeight, 80, '#ffffff');

for (let i = 0; i < 6; i++) {
	const cone = new Cone(Math.random() * innerWidth, Math.random() * innerHeight, 60, '#ffffff');
	cones.push(cone);
}

animateMap();
