function displayQuestion(questionObj) {
	const questionHeading = document.getElementById('questionHeading');
	const question = document.getElementById('question');
	const ansA = document.getElementById('answerAContainer');
	const ansB = document.getElementById('answerBContainer');
	const ansC = document.getElementById('answerCContainer');
	const ansD = document.getElementById('answerDContainer');

	questionHeading.innerText = 'QUESTION ' + questionObj.qNumber;
	question.innerText = questionObj.qText;
	ansA.innerText = 'A: ' + questionObj.answers[0];
	ansB.innerText = 'B: ' + questionObj.answers[1];
	ansC.innerText = 'C: ' + questionObj.answers[2];
	ansD.innerText = 'D: ' + questionObj.answers[3];
}

const testQuestions = [
	{
	  'qNumber': 1,
	  'qText': "What is the difference between Git and GitHub?",
	  'answers': [
		"Version control system",
		"Web-based hosting platform",
		"Source code management tool",
		"Collaboration platform"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 2,
	  'qText': "What is Agile software development?",
	  'answers': [
		"Sequential and rigid approach",
		"Documentation-heavy process",
		"Individual work",
		"Iterative and flexible approach"
	  ],
	  'correctAnswerIndex': 3
	},
	{
	  'qNumber': 3,
	  'qText': "What is object-oriented programming (OOP)?",
	  'answers': [
		"Programming paradigm for organizing code",
		"Approach that doesn't use classes",
		"Technique used in low-level languages",
		"Style that only allows static methods"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 4,
	  'qText': "What is the purpose of a constructor in object-oriented programming?",
	  'answers': [
		"To destroy objects",
		"To override the behavior of the parent class",
		"To define private variables in a class",
		"To initialize objects and set their initial state"
	  ],
	  'correctAnswerIndex': 3
	},
	{
	  'qNumber': 5,
	  'qText': "What is the difference between synchronous and asynchronous programming?",
	  'answers': [
		"Tasks executed one after the other",
		"Tasks executed concurrently",
		"Sequential execution",
		"Concurrent execution"
	  ],
	  'correctAnswerIndex': 1
	},
	{
	  'qNumber': 6,
	  'qText': "What is the purpose of a foreign key in a database?",
	  'answers': [
		"Establish referential integrity",
		"Define primary key",
		"Index columns",
		"Backup and recovery"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 7,
	  'qText': "What is the role of a software tester?",
	  'answers': [
		"Ensure software quality",
		"Write code",
		"Manage projects",
		"Design user interface"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 8,
	  'qText': "What is the purpose of version control in software development?",
	  'answers': [
		"Track changes to source code",
		"Generate documentation",
		"Optimize performance",
		"Manage licenses"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 9,
	  'qText': "What is the difference between a variable and a constant in programming?",
	  'answers': [
		"Variable: mutable, Constant: immutable",
		"Variable: immutable, Constant: mutable",
		"Variable: store strings, Constant: store numbers",
		"Variable: changeable, Constant: unchangeable"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 10,
	  'qText': "What is the purpose of a package manager in software development?",
	  'answers': [
		"Automate software installation",
		"Measure performance",
		"Convert code to machine",
		"Authenticate users"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 11,
	  'qText': "What is the difference between a stack and a queue in data structures?",
	  'answers': [
		"Stack: Last-In-First-Out, Queue: First-In-First-Out",
		"Stack: First-In-First-Out, Queue: Last-In-First-Out",
		"Stack: Last-In-First-Out, Queue: Last-In-First-Out",
		"Stack: First-In-First-Out, Queue: First-In-First-Out"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 12,
	  'qText': "What is the purpose of unit testing in software development?",
	  'answers': [
		"Verify code correctness",
		"Monitor performance",
		"Simulate user interactions",
		"Generate automated documentation"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 13,
	  'qText': "What is the role of a database administrator (DBA)?",
	  'answers': [
		"Design and maintain databases",
		"Write SQL queries",
		"Develop user interfaces",
		"Manage network infrastructure"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 14,
	  'qText': "What is the purpose of a user interface (UI) in software applications?",
	  'answers': [
		"Enable user interaction",
		"Convert code to machine",
		"Manage database transactions",
		"Perform automated testing"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 15,
	  'qText': "What is the difference between static and dynamic typing in programming languages?",
	  'answers': [
		"Static: compile-time, Dynamic: runtime",
		"Static: runtime, Dynamic: compile-time",
		"Static: compile-time type checking",
		"Static: runtime type checking"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 16,
	  'qText': "What is the purpose of an API (Application Programming Interface)?",
	  'answers': [
		"Enable software communication",
		"Define database schemas",
		"Deploy software applications",
		"Validate user input"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 17,
	  'qText': "What is the role of a software architect?",
	  'answers': [
		"Design software structure",
		"Write code",
		"Test software quality",
		"Manage project timelines"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 18,
	  'qText': "What is the purpose of a loop in programming?",
	  'answers': [
		"Repeat code execution",
		"Terminate program",
		"Define variables",
		"Handle user input"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 19,
	  'qText': "What is the difference between front-end and back-end development?",
	  'answers': [
		"Front-end: User interface, Back-end: Server-side programming",
		"Front-end: Server-side programming, Back-end: User interface",
		"Front-end: Database management, Back-end: User interface",
		"Front-end: User experience, Back-end: Database management"
	  ],
	  'correctAnswerIndex': 0
	},
	{
	  'qNumber': 20,
	  'qText': "What is the purpose of a bug tracking system in software development?",
	  'answers': [
		"Record, track, and manage software defects",
		"Generate automated test cases",
		"Monitor software performance",
		"Manage software licenses"
	  ],
	  'correctAnswerIndex': 0
	}
  ];
  
  
// remove later - just for testing now
var randIndex =  Math.floor(Math.random() * testQuestions.length)
displayQuestion(testQuestions[randIndex]);
  