let players = [];
const playersToAdd = [
	'Backfire',
	'Destiny',
	'Hunter',
	'Pain',
	'Thunder',
	'Wolf',
	'Elite',
	'Fury',
	'Titan',
	'Impact',
	'One',
	'Rage',
	'Sharp',
	'Strike',
	'Lightning',
	'King',
	'Master',
	'Phantom',
	'Reaper',
	'Warrior',
	'Aim',
	'Blink',
	'Boom',
	'Chaos',
	'Crush',
	'Death',
	'Devastation',
	'Echo',
	'Fire',
	'Gold',
	'Ice',
	'Jackal',
	'Justice',
	'Legend',
	'Overkill',
	'Shield',
	'Silence',
	'Sniper',
	'Soldier',
	'Storm',
	'Alpha',
	'Beast',
	'Oдин ',
	'Fire',
	'Flash',
	'Ghost',
];

const playerContainer = document.getElementById('conectedPlayerContainer');
const playerCounter = document.getElementById('numPlayers');

function generatePlayerCards() {
	playerContainer.innerHTML = '';
	players.forEach((player) => {
		const playerCard = document.createElement('div');
		playerCard.classList.add('connectedPlayer');
		playerCard.textContent = player;
		playerContainer.appendChild(playerCard);
		playerCounter.innerHTML = 'Players connected: ' + players.length;
	});
}

let count = 0;

// setInterval(() => {
// 	if (count < playersToAdd.length) {
// 		players.push(playersToAdd[count++]);
// 		generatePlayerCards();
// 	}
// }, 1000);

generatePlayerCards();

const startGameButton = document.getElementById('startGameButton');

startGameButton.addEventListener('click', () => {
	console.log('STARTING GAME');
	dispatch('startGame');
});
