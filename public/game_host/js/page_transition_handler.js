const backgroundAnimation = document.getElementById('animationCanvas');
const splashscreen = document.getElementById('splashscreenContainer');
const lobby = document.getElementById('lobbyPage');
const gamePage = document.getElementById('gamePage');
const digitalMap = document.getElementById('digitalMap');
const quizPage = document.getElementById('quizPage');

const debug = true;

backgroundAnimation.style.display = debug ? 'none':'flex';
splashscreen.style.display = debug ? 'none':'flex';
lobby.style.display = !debug ? 'none':'flex';
gamePage.style.display = debug ? 'none':'flex';

let currentPage = splashscreen;

function transitionToPage(page, showBackgroundAnimation) {
	const blurredTransitionLayer = document.createElement('div');
	blurredTransitionLayer.id = 'blurredTransitionLayer';

	const clippedTransitionLayer = document.createElement('div');
	clippedTransitionLayer.id = 'clippedTransitionLayer';
	clippedTransitionLayer.style.backgroundColor = '#111';
	clippedTransitionLayer.style.clipPath = 'circle(0% at 50% 50%)';

	const transitionContainer = document.createElement('div');

	const layers = [blurredTransitionLayer, clippedTransitionLayer, transitionContainer];
	layers.forEach(function (layer) {
		layer.style.position = 'absolute';
		layer.style.top = '0';
		layer.style.left = '0';
		layer.style.width = '100vw';
		layer.style.height = '100vh';
	});

	// Append the divs to the document body

	transitionContainer.appendChild(clippedTransitionLayer);
	transitionContainer.appendChild(blurredTransitionLayer);

	document.body.appendChild(transitionContainer);

	let blur = 0;
	let radius = 0;
	let opacity = 1;

	let transitionInterval = setInterval(() => {
		if (radius < 100) {
			radius += 0.15;
			blur += 0.4;
			opacity -= 0.005;
			if (opacity < 0) opacity = 0;

			clippedTransitionLayer.style.clipPath = `circle(${radius}% at ${50}% ${50}%)`;
			blurredTransitionLayer.style.backdropFilter = `blur(${blur}px)`;
			currentPage.style.opacity = opacity;
			clippedTransitionLayer.style.opacity = 1 - opacity;
		} else {
			clearInterval(transitionInterval);
			currentPage.style.display = 'none';
			currentPage = page;
			page.style.display = 'flex';
			page.style.opacity = 1;
			opacity = 1;
			blurredTransitionLayer.style.backdropFilter = 'blur(0)';

			if (showBackgroundAnimation) {
				backgroundAnimation.style.display = 'flex';
				particles.splice(0);
			} else {
				backgroundAnimation.style.display = 'none';
			}

			transitionInterval = setInterval(() => {
				if (opacity > 0) {
					opacity -= 0.005;

					clippedTransitionLayer.style.opacity = opacity;
				} else {
					clearInterval(transitionInterval);
					document.body.removeChild(transitionContainer);
				}
			}, 1);
		}
	}, 1);
}
