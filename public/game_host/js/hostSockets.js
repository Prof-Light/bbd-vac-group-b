const socket = io('http://localhost:3000');

//==================================================
// SEE public/player_web_app/js/playerSockets.js
// The format is basically the same for the below dicts
//==================================================

var hostNameHeader = null;

window.onload = () => {
	hostNameHeader = document.getElementById('hostNameHeader');
};

// This is the unique identifier for the current host.
// Will be used when we support multiple hosts
// TODO: Allow the host to specify their own id before starting
var hostName = 'default';

// Since we're not using typescript, please specify the data types
// you are sending in the params for each of these functions :)
// You can use pseudo-typescript syntax I guess
const sendCommands = {
	// Called when the host initially connects to the cloud server
	// * returns: string
	'hostConnected': (_) => {
		if (hostNameHeader) hostNameHeader.innerHTML = 'Connected as host: ' + hostName;
		return { hostName };
	},
	'startGame': (_) => {
		console.log('START GAME');
		return '';
	},
};

// These are events that you want to listen for on the host laptop browser
// Don't change this at runtime, all events should be defined at the start
const receiveEvents = {
	// * receives: string
	'pageChangeState': (pageName) => {
		switch (pageName) {
			case 'gamePage':
				transitionToPage(gamePage, false);
				break;

			case 'lobby':
				transitionToPage(lobby, true);

				break;
			case 'digitalMap':
				break;

			case 'quizScreen':
				break;
			default:
				break;
		}

		transitionToPage(page, true);
	},

	// * receives: array of string
	'lobbyPlayerUpdate': (playerNames) => {
		players = playerNames;
		generatePlayerCards();
	},

	// * receives: object
	'objectPositionUpdate': (objectPositions) => {},

	// * receives: number, string
	'newQuestion': (questionObj) => {
		displayQuestion(questionObj);
	},
};

//===== SOCKET CONFIG STUFF =====//
// You can mostly ignore this

var socketId = null;

socket.on('connect', function () {
	console.log('Connected to the server');
	socketId = socket.id;
	socket.emit('ehlo', 'host'); // This tells the server that this is a host
	dispatch('hostConnected', null);
});

socket.on('disconnect', function () {
	socketId = null;
	if (hostNameHeader) hostNameHeader.innerHTML = 'Not connected';
	console.log('Disconnected from the server');
});

// Auto close the socket connection when the window is closed
window.onbeforeunload = function () {
	socket.disconnect();
	console.log('Socket disconnected due to page unload');
};

function getClientData() {
	return {
		socketId,
		hostName,
	};
}

// This is the function that you call to dispatch one of the above sendCommands
function dispatch(commandName, data) {
	console.log('Sending event:', commandName, data);
	if (sendCommands[commandName]) {
		const clientData = getClientData();
		const payload = { clientData, data: sendCommands[commandName](data) };
		if (payload !== null) {
			socket.emit('host/' + commandName, payload);
		}
	}
}

// Start listening for receiveEvents
for (const eventName in receiveEvents) {
	if (!receiveEvents.hasOwnProperty(eventName)) continue;

	socket.on(eventName, (data) => {
		receiveEvents[eventName](data);
	});
}
