const animationCanvas = document.getElementById('animationCanvas');
const animationCtx = animationCanvas.getContext('2d');
const particles = [];
const maxParticles = 20;
const accentColour1 = 'rgb(200, 0, 0)';
const accentColour2 = 'rgb(180, 180, 180)';
const backgroundColour = '#111';
const alphaReduction = 0.005;
const minBrightness = 50;
const maxBrightness = 200;
let minRadius = innerWidth / 8;
let maxRadius = innerWidth / 6;
let minSpeed = innerWidth / 10000;
let maxSpeed = innerWidth / 6000;
let pushInterval;

animationCanvas.width = innerWidth;
animationCanvas.height = innerHeight;

class Particle {
	constructor(x, y, radius, color, velocity, acceleration) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.color = color;
		this.velocity = velocity;
		this.acceleration = acceleration;
		this.alpha = 1;
		this.alphaReduction = 0.002;
	}

	draw() {
		animationCtx.save();
		animationCtx.globalAlpha = this.alpha;
		animationCtx.beginPath();
		animationCtx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
		animationCtx.fillStyle = this.color;
		animationCtx.fill();
		animationCtx.restore();
	}

	update() {
		this.draw();
		this.x += this.velocity.x;
		this.y += this.velocity.y;
		this.velocity.x += this.acceleration.x;
		this.velocity.y += this.acceleration.y;
		this.alpha -= this.alphaReduction;
	}
}

function animateSplashscreen() {
	animationCtx.fillStyle = backgroundColour;
	animationCtx.fillRect(0, 0, animationCanvas.width, animationCanvas.height);

	particles.forEach((particle, index) => {
		if (particle.alpha <= 0) {
			setTimeout(() => {
				particles.splice(index, 1);
			}, 0);
		} else {
			particle.update();
		}
	});
}

function pushParticle() {
	if (particles.length < maxParticles) {
		let x;
		let y;

		let radius = Math.random() * (maxRadius - minRadius) + minRadius;
		let angle;

		if (Math.random() < 0.5) {
			if (Math.random() < 0.5) {
				x = 0 - radius;
				angle = (Math.random() - 0.5) * Math.PI;
			} else {
				x = animationCanvas.width + radius;
				angle = (Math.random() + 0.5) * Math.PI;
			}
			y = Math.random() * animationCanvas.height;
		} else {
			x = Math.random() * animationCanvas.width;
			if (Math.random() < 0.5) {
				y = 0 - radius;
				angle = Math.random() * Math.PI;
			} else {
				y = animationCanvas.height + radius;
				angle = (Math.random() - 1) * Math.PI;
			}
		}

		let speed = Math.random() * (maxSpeed - minSpeed) + minSpeed;

		let velocity = {
			x: Math.cos(angle) * speed,
			y: Math.sin(angle) * speed,
		};

		let acceleration = {
			x: 0,
			y: 0,
		};

		let colour;

		if (Math.random() < 1) {
			if (Math.random() < 0.6) {
				colour = accentColour1;
			} else {
				colour = accentColour2;
			}
		} else {
			let brightness = Math.random() * (maxBrightness - minBrightness) + minBrightness;
			colour = `rgb(${brightness}, ${brightness}, ${brightness})`;
		}

		particles.push(new Particle(x, y, radius, colour, velocity, acceleration));
	}
}

let running = false;
let direction = 0;

const timeCut = 0;

addEventListener('click', () => {
	if (!running) {
		running = true;
		playSplashscreen();
	}
});

let opacity = 1;
const welcomeHeading = document.getElementById('welcome-heading');
welcomeHeading.style.opacity = 1;

let fadeInterval = setInterval(() => {
	if (opacity >= 0 && opacity <= 1) {
		if (direction) {
			opacity += 0.01;
			welcomeHeading.style.opacity = opacity;
		} else {
			opacity -= 0.01;
			welcomeHeading.style.opacity = opacity;
		}
	} else {
		if (direction) {
			direction = false;
			opacity -= 0.01;
		} else {
			direction = true;
			opacity += 0.01;
		}
	}
}, 20);

function playSplashscreen() {
	clearInterval(fadeInterval);

	fadeInterval = setInterval(() => {
		if (opacity > 0) {
			opacity -= 0.01;
			welcomeHeading.style.opacity = opacity;
		} else {
			clearInterval(fadeInterval);
			opacity = 0;
			welcomeHeading.style.opacity = opacity;
			welcomeHeading.innerHTML = 'Team B Presents';

			fadeInterval = setInterval(() => {
				if (opacity < 1) {
					opacity += 0.01;
					welcomeHeading.style.opacity = opacity;
				} else {
					clearInterval(fadeInterval);

					console.log('here');
					const audio = new Audio('game_host/audio/sphero_ball_1.mp3');
					audio.currentTime = timeCut;
					audio.volume = 0;
					audio.play();

					let volumeInterval = setInterval(() => {
						if (audio.volume < 0.99) {
							audio.volume += 0.001;
						} else {
							clearInterval(volumeInterval);
							audio.volume = 1;
						}
					}, 10);

					setTimeout(() => {
						audio.currentTime = 64;

						setInterval(() => {
							audio.currentTime = 64;
						}, 32000);
					}, 96000 - timeCut * 1000);

					setTimeout(() => {
						transitionHeading();
					}, 5600 - timeCut * 1000);

					setTimeout(() => {
						if (!debug) {
							transitionToPage(lobby, true);
						}
					}, 16000);
				}
			}, 1);
		}
	}, 1);
}

function transitionHeading() {
	const heading = document.getElementById('welcome-heading');

	const fadeOutInterval = setInterval(() => {
		if (opacity > 0) {
			opacity -= 0.01;
			heading.style.opacity = opacity;
		} else {
			clearInterval(fadeOutInterval);

			setTimeout(() => {
				heading.style.fontSize = '16vmin';
				heading.style.fontWeight = '400';
				heading.innerHTML = 'Sphero Ball';

				const fadeInInterval = setInterval(() => {
					if (opacity < 1) {
						opacity += 0.0008;
						heading.style.opacity = opacity;
					} else {
						clearInterval(fadeInInterval);
					}
				}, 1);

				setInterval(() => {
					animateSplashscreen();
				}, 16);

				setInterval(() => {
					pushParticle();
				}, 50);
			}, 2000);
		}
	}, 1);
}
