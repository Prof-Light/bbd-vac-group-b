const { spawn } = require('child_process');

function sendCommand(cmd) {
  const python = spawn('python3', ['./public/sphero_webcam_controller/js/move_sphere.py']);
  python.stdout.on('data', (data) => {
    console.log(data.toString());
  });
}

exports.sendCommand = sendCommand;
