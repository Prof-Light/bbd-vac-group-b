const { Server } = require('socket.io');
const express = require('express');
const http = require('http');
// Express endpoint server
const app = express();

// Socket.io server
const sockets = http.createServer(app);
const io = new Server(sockets, { cors: { origin: '*' } });

var browserConnection = null;

io.on("connection", (socket) => {
  if (browserConnection === null) {
    browserConnection = socket.id;
    console.log(`sphero server connected to host ${socket.id}`);
  
    socket.on("moveSphero", (...degrees) => {
      // run python to move sphero ball
      console.log("received movement: " + degrees);
    });
  
    socket.on("disconnect", () => {
      browserConnection = null;
      console.log("socket disconnected");
    });
  }
});

if (process.env.PORT == null) {
	sockets.listen(6969, () => {
		console.log('listening on port 6969');
	});
} else {
	sockets.listen(process.env.PORT, () => console.log(`Server is active on port: ${process.env.PORT}`));
}